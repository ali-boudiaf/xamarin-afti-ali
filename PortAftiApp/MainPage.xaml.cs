﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PortAftiApp.Model;
using Xamarin.Forms;

namespace PortAftiApp
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        //List<Port> ports;

        public MainPage()
        {
            InitializeComponent();

            const string URL = "https://drive.google.com/uc?export=download&id=1doOUR4AtBF56uvtqFaz_iTIfYDc8mrti";


            using (var webClient = new WebClient())
            {
                try
                {

                    webClient.DownloadStringCompleted += (object sender, DownloadStringCompletedEventArgs e) =>
                    {
                        string portsJson = e.Result;

                        List<Port> ports = JsonConvert.DeserializeObject<List<Port>>(portsJson);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            listView.ItemsSource = ports;
                        });

                    };
                    webClient.DownloadStringAsync(new Uri(URL));
                }
                catch (Exception ex)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayAlert("Erreur", "Problème réseau : " + ex.Message, "OK");
                    });

                    return;
                }

            }

        }
    }
}
