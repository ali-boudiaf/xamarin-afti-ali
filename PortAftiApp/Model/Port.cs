﻿using System;
namespace PortAftiApp.Model
{
    public class Port
    {
        // id, nom, lat, long, gazole, sp98, dat
        public string id { get; set; }
        public string nom { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public double gazole { get; set; }
        public double sp98 { get; set; }
        public DateTime Date { get; set; }

        public string PrixGazole { get { return "Gazole : " +  gazole + "€"; } }
        public string PrixSp98 { get { return "SP98 : " + sp98 + "€"; } }

        public Port()
        {
        }
    }
}
