# Rapport projet mobile Xamarin
> Ali Boudiaf - ETGL - P56

Développement d'une application mobile pour Android à l'aide du framework Xamarin.

### Table des matières

<!-- vscode-markdown-toc -->
* 1. [Hébergement et versionning du code](#Hbergementetversionningducode)
* 2. [Installation de l’environnement](#Installationdelenvironnement)
* 3. [Générer des fixture](#Gnrerdesfixture)
* 4. [Client serveur](#Clientserveur)
* 5. [Gestion de l’asynchrone](#Gestiondelasynchrone)
* 6. [Désérialiser](#Dsrialiser)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


##  1. <a name='Hbergementetversionningducode'></a>Hébergement et versionning du code 

Projet initialisé avec Visual studio. Puis créé sur un repository distant hébergé sur la plateforme Gitlab. Le projet dispose de deux branches:
Develop, branche dédiée au développement
De cette branche d’autres branches pourront être ajoutées au fur et à mesure du développement de l’application mobile. Puis seront supprimées au fur et à mesure des merges request.

Master, branche dédiée à la production. Uniquement le code dans sa version finale.

##  2. <a name='Installationdelenvironnement'></a>Installation de l’environnement

Installation de l’environnement

L’ide utilisé est Visual Studio, l’ajout de certains plugins facilitent la gestion de certains format comme le format Json comme par exemple le plugin Newtonsoft.Json.

L’ajout des plugins est facilité par le gestionnaire NuGet.

Enfin la configuration d’un émulateur de téléphone android à était nécessaire. Ainsi que l’ajout du kit de développement. 

Création d’une classe :

````C#
using System;
namespace PortAftiApp.Model
{
    public class Port
    {
        // id, nom, lat, long, gazole, sp98, dat
        public string id { get; set; }
        public string nom { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public double gazole { get; set; }
        public double sp98 { get; set; }
        public DateTime Date { get; set; }

        public string PrixGazole { get { return "Gazole : " +  gazole + "€"; } }
        public string PrixSp98 { get { return "SP98 : " + sp98 + "€"; } }

        public Port()
        {
        }
    }
}
````

Il s’agit de l’entité qui représente la classe principale de l’application. Ses méthodes sont en public ce qui permet d’y accéder plus facilement depuis d'autre fichier de répertoire sources. Notamment par la classe main qui s’occupe de l’affichage de l’application sur mobile.

Voici le code source de la classe main :

````C#
<?xml version="1.0" encoding="utf-8"?>
<ContentPage xmlns="http://xamarin.com/schemas/2014/forms" xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml" xmlns:d="http://xamarin.com/schemas/2014/forms/design" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" mc:Ignorable="d" x:Class="PortAftiApp.MainPage">
    <NavigationPage.TitleView>
        <StackLayout HorizontalOptions="Center" 
				Margin="{OnPlatform iOS='0,0,25,0', Android='0,0,20,0', Default=0}"
				Orientation="Horizontal">

            <!--<Image Source="pizza_logo.png" HeightRequest="40" />-->
            <Label Text="Ports" 
			FontAttributes="Bold"
			TextColor="White" VerticalOptions="Center" />
        </StackLayout>


    </NavigationPage.TitleView>


    <ListView x:Name="listView" RowHeight="100">
        <ListView.ItemTemplate>
            <DataTemplate>
                <ViewCell>
                    <Grid>
                        <Grid.RowDefinitions>
                            <RowDefinition Height="*" />
                        </Grid.RowDefinitions>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="Auto" />
                            <!--<ColumnDefinition Width="*" />
                            <ColumnDefinition Width="50" />-->
                        </Grid.ColumnDefinitions>

                        <StackLayout Grid.Row="0" Grid.Column="0" Margin="10, 0, 0, 0" VerticalOptions="Center" HorizontalOptions="FillAndExpand" Orientation="Vertical">
                            <Label Text="{Binding nom}" TextColor="Black" FontSize="24"/>
                            <Label Text="{Binding PrixGazole}" TextColor="Black" FontSize="16"/>
                            <Label Text="{Binding PrixSp98}" TextColor="Black" FontSize="16"/>
                        </StackLayout>
                    </Grid>
                </ViewCell>
            </DataTemplate>
        </ListView.ItemTemplate>
    </ListView>


</ContentPage>
````
> ### Remarques concernant la classe main
> L'application affiche une liste des prix d'un port, c'est pour cette raison que le composant ```<ListView></ListView>``` à été utilisé.
> Un système de Grid a été utilisé pour remplir l'espace de l'écran correctement.



##  3. <a name='Gnrerdesfixture'></a>Générer des fixtures

L’application devant dans sa version finale consommer les services d’une API il fallait développer ses différentes parties autour de fausses données.

J’ai donc généré des fixtures au format Json.

Après avoir pris en compte le schéma de la future base de données, j’ai opté pour un choix d’organisation des données.

Ainsi à l’aide de l’outil en ligne https://www.json-generator.com/# j’ai pu générer autant de ligne que nécessaire pour commencer à travailler.

En effet après avoir rentré des clés, json generator propose l’utilisation de fonction pour générer des fausse données.

Par exemple en utilisant la fonction ```objectId()```, il y aura autant d’id créé que de ligne demandé. 
La fonction ````repeat(10)````, initialisera 10 lignes avec des données aléatoirement. En ajustant les entrées de chaque fonction on obtient un script fonctionnel.

Donc voici le script dans sa version final, qui générera 10 entrées avec
id, nom, latitude, longitude, prix gazole et prix sp98 généré aléatoirement.

````javascript
[
  '{{repeat(10)}}',
  {
    id: '{{objectId()}}',
    nom: '{{surname()}}',
    latitude: '{{floating(-90.000001, 90)}}',
    longitude: '{{floating(-180.000001, 180)}}',
    gazole: '{{floating(0.10, 2.50, 2)}}',
    sp98: '{{floating(0.10, 2.50, 2)}}'
  }
]
````

Et voici un aperçu des fixtures pouvant être utilisées dans l'application :

````javascript
[
  {
    "id": "5ebd454a6ea7a7b21526bec9",
    "nom": "Becker",
    "latitude": -29.639976,
    "longitude": 0.855185,
    "gazole": 1.57,
    "sp98": 0.59
  },
  {
    "id": "5ebd454af8bca870baac814e",
    "nom": "Ware",
    "latitude": -18.321896,
    "longitude": -52.302655,
    "gazole": 1.12,
    "sp98": 1.63
  },
  // ...
  // ...
  // ...
  {
    "id": "5ebd454a476566dff43594e5",
    "nom": "Caldwell",
    "latitude": -13.950388,
    "longitude": -144.674917,
    "gazole": 1.83,
    "sp98": 0.7
  }
]
````

##  4. <a name='Clientserveur'></a>Client serveur 

L’application mobile consomme les service d’une api.

En production l’application récupérera les données depuis un serveur exposant une API.

En développement, l’application récupère sur un serveur google drive, un fichier contenant les fausses données.

Les fausses données utilisé pour le développement sont disponnibles via cette url : "https://drive.google.com/uc?export=download&id=1doOUR4AtBF56uvtqFaz_iTIfYDc8mrti";

Les données sont au format Json.

Côté application le code source permettant l’appel reste le même que ce soit en production ou en développement. Ainsi il y a un appel qui est fait à l’aide de ce code dans un bloc try catch :

````C#
const string URL = "https://drive.google.com/uc?export=download&id=1doOUR4AtBF56uvtqFaz_iTIfYDc8mrti";


            using (var webClient = new WebClient())
            {
                try
                {

                    webClient.DownloadStringCompleted += (object sender, DownloadStringCompletedEventArgs e) =>
                    {
                        string portsJson = e.Result;

                        List<Port> ports = JsonConvert.DeserializeObject<List<Port>>(portsJson);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            listView.ItemsSource = ports;
                        });

                    };
                    webClient.DownloadStringAsync(new Uri(URL));
                }
                catch (Exception ex)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayAlert("Erreur", "Problème réseau : " + ex.Message, "OK");
                    });

                    return;
                }
`````
> ### Note concernant la récupération des données:
> Il s'agit d'un appel vers l'extérieur de l'application, ceci peut être sujet à des erreurs. C'ets pourquoi l'utilisation d'un block try/catch à été nécessaire. L'idée est de capturer l'erreur s'il y a et de la gérer.

Voici le résultat une fois les données chargée dans l'application depuis le serveur. On y voit une liste contruite autour des données.

![image](/ports-afti.png =200x400)

##  5. <a name='Gestiondelasynchrone'></a>Gestion de l’asynchrone 

Lors de la récupération des données depuis un serveur l'application ne doit pas rester bloquer, ainsi les méthodes async/await ont étaient utilisé.

Celà permet à l'application de pouvoir poursuivre son cycle de vie et en même temps laisser tourner une tâche devant dans cet exercice récupérér des données distante. On parle de gestion de l'asynchrone.

Voici le code qui à permit l'asynchrone :

````C#
webClient.DownloadStringAsync(new Uri(URL));
`````
> ### Note:
> Il existe plusieurs variante de cette fonction, celle-ci est dédiée à la récupération de donnée enrigistrée dans un fichier. Une fois récupérées, les données doivent être traitées. Pour cela il est nécessaire de les transformet en Objets manipulable, on parle de désérialiser.

##  6. <a name='Dsrialiser'></a>Désérialiser les données

Les données reçu doivent être convertit en objet manipulable à travers les méthodes de classes, c'est à l'aide d'un plugin permettant de gérer les données Json que cette opération à était rendu possible.

Le plugin Json converter dispose d'une fonction permettant cette désérialisation voici le code permettant cette action :

````C#
List<Port> ports = JsonConvert.DeserializeObject<List<Port>>(portsJson);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            listView.ItemsSource = ports;
                        });
`````
> ### Note 
> La fonction ````JsonConvert.DeserializeObject<List<Port>>(portsJson)```` prend en paramètre les données aux format Json et les transforme en une liste d'objets.
